json.extract! event, :id, :title, :summary, :link, :created_at, :updated_at
json.url event_url(event, format: :json)
